package controller

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/daakghar-service-gin/api/presenter"
	"gitlab.com/daakghar-service-gin/entity"
	"gitlab.com/daakghar-service-gin/service"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ActivityController interface {
	FindById(ctx *gin.Context) (presenter.Activity, error)
	FindAll() []presenter.Activity
	Save(ctx *gin.Context) (interface{}, error)
	Update(ctx *gin.Context) (interface{}, error)
	Delete(ctx *gin.Context) (interface{}, error)
}

type activityController struct {
	service service.ActivityService
}

// var validate *validator.Validate

func NewActivityController(service service.ActivityService) ActivityController {
	// validate = validator.New()
	// validate.RegisterValidation("is-cool", validators.ValidateCoolTitle)
	return &activityController{
		service: service,
	}
}

func (c *activityController) FindById(ctx *gin.Context) (presenter.Activity, error) {

	itemId := ctx.Param("id")
	dbActivity, err := c.service.FindById(itemId)

	if err != nil {
		return presenter.Activity{}, err
	}

	var jActivity presenter.Activity
	jActivity.FromDB(dbActivity)

	return jActivity, nil

}

func (c *activityController) FindAll() []presenter.Activity {

	entityActivitys := c.service.FindAll()

	var jResp []presenter.Activity
	var jActivity presenter.Activity

	for _, activity := range entityActivitys {
		jActivity.FromDB(activity)
		jResp = append(jResp, jActivity)
	}

	return jResp
}

func (c *activityController) Save(ctx *gin.Context) (interface{}, error) {
	var jActivity presenter.Activity
	var dbActivity entity.Activity

	err := ctx.ShouldBindJSON(&jActivity)
	if err != nil {
		return nil, err
	}

	err = jActivity.ToDB(&dbActivity)
	if err != nil {
		return nil, err
	}
	dbActivity.PreparePut()

	// err = validate.Struct(activity)
	// if err != nil {
	// 	return err
	// }

	return c.service.Save(dbActivity)
}

func (c *activityController) Update(ctx *gin.Context) (interface{}, error) {

	itemId := ctx.Param("id")

	var jActivity presenter.Activity
	var dbActivity entity.Activity

	err := ctx.ShouldBindJSON(&jActivity)
	if err != nil {
		return nil, err
	}

	err = jActivity.ToDB(&dbActivity)
	if err != nil {
		return nil, err
	}

	objectID, err := primitive.ObjectIDFromHex(itemId)
	if err != nil {
		return nil, err
	}
	dbActivity.ID = objectID
	dbActivity.PreparePut()

	// err = validate.Struct(activity)
	// if err != nil {
	// 	return err
	// }

	return c.service.Update(itemId, dbActivity)
}

func (c *activityController) Delete(ctx *gin.Context) (interface{}, error) {

	id := ctx.Param("id")

	return c.service.Delete(id)

}

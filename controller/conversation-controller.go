package controller

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/daakghar-service-gin/api/presenter/conversation"
	"gitlab.com/daakghar-service-gin/entity"
	"gitlab.com/daakghar-service-gin/service"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ConversationController interface {
	FindById(ctx *gin.Context) (conversation.Get, error)
	FindAll() []conversation.Get
	Save(ctx *gin.Context) (interface{}, error)
	Update(ctx *gin.Context) (interface{}, error)
	Delete(ctx *gin.Context) (interface{}, error)
}

type conversationController struct {
	service service.ConversationService
}

// var validate *validator.Validate

func NewConversationController(service service.ConversationService) ConversationController {
	// validate = validator.New()
	// validate.RegisterValidation("is-cool", validators.ValidateCoolTitle)
	return &conversationController{
		service: service,
	}
}

func (c *conversationController) FindById(ctx *gin.Context) (conversation.Get, error) {

	itemId := ctx.Param("id")
	dbConversation, err := c.service.FindById(itemId)

	if err != nil {
		return conversation.Get{}, err
	}

	var jConversation conversation.Get
	jConversation.FromDB(dbConversation)

	return jConversation, nil

}

func (c *conversationController) FindAll() []conversation.Get {

	entityConversations := c.service.FindAll()

	var jResp []conversation.Get
	var jConversation conversation.Get

	for _, conversation := range entityConversations {
		jConversation.FromDB(conversation)
		jResp = append(jResp, jConversation)
	}

	return jResp
}

func (c *conversationController) Save(ctx *gin.Context) (interface{}, error) {
	var jConversation conversation.Create
	var dbConversation entity.Conversation

	err := ctx.ShouldBindJSON(&jConversation)
	if err != nil {
		return nil, err
	}

	err = jConversation.ToDB(&dbConversation)
	if err != nil {
		return nil, err
	}
	dbConversation.PreparePut()

	// err = validate.Struct(conversation)
	// if err != nil {
	// 	return err
	// }

	return c.service.Save(dbConversation)
}

func (c *conversationController) Update(ctx *gin.Context) (interface{}, error) {

	itemId := ctx.Param("id")

	var jConversation conversation.Update
	var dbConversation entity.Conversation

	err := ctx.ShouldBindJSON(&jConversation)
	if err != nil {
		return nil, err
	}

	err = jConversation.ToDB(&dbConversation)
	if err != nil {
		return nil, err
	}

	objectID, err := primitive.ObjectIDFromHex(itemId)
	if err != nil {
		return nil, err
	}
	dbConversation.ID = objectID
	dbConversation.PreparePut()

	// err = validate.Struct(conversation)
	// if err != nil {
	// 	return err
	// }

	return c.service.Update(itemId, dbConversation)
}

func (c *conversationController) Delete(ctx *gin.Context) (interface{}, error) {

	id := ctx.Param("id")

	return c.service.Delete(id)

}

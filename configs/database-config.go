package configs

import "gitlab.com/daakghar-service-gin/environment"

const (
	mongodbLocalClientPort int    = 27017
	mongodbLocalUri        string = "mongodb://localhost:27017"
	mongodbAtlasUri        string = "mongodb+srv://zobayer94:seagoldbag@cluster0.uja0q.mongodb.net/daakghar?retryWrites=true&w=majority"
)

func GetMongodbUri(envName string) string {
	if envName == environment.Heroku {
		return mongodbAtlasUri
	}

	return mongodbLocalUri
}

# daakghar-service-gin

## Go Module Init

```bash
go mod init gitlab.com/mdzobayer/daakghar-service-gin
```

## Gin-Gonic library: github.com/gin-gonic/gin

## Run

```bash
go run main.go
```

# Swagger Documentation

## Install Swagger Library

```bash
go get -u github.com/swaggo/swag/cmd/swag
```

## Generate Swagger Documentation

```bash
swag init
```

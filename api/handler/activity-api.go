package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/daakghar-service-gin/controller"
)

type ActivityApi struct {
	ActivityController controller.ActivityController
}

func NewActivityAPI(
	ActivityController controller.ActivityController) *ActivityApi {
	return &ActivityApi{
		ActivityController: ActivityController,
	}
}

// Path Information

// GetActivities godoc
// @Security bearerAuth
// @Summary List existing Activitys
// @Description Get all the existing Activitys
// @Tags Activitys, list
// @Accept json
// @Produce json
// @Success 200 {array} presenter.Activity
// @Failure 401 {object} dto.Response
// @Router /activities [get]
func (api *ActivityApi) GetActivities(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, api.ActivityController.FindAll())
}

// GetActivity godoc
// @Security bearerAuth
// @Summary Get existing Activity
// @Description Get a existing Activitys
// @Tags Activitys
// @Accept json
// @Produce json
// @Param id path string true "Activity ID"
// @Success 200 {object} presenter.Activity
// @Failure 401 {object} dto.Response
// @Router /activities/{id} [get]
func (api *ActivityApi) GetActivity(ctx *gin.Context) {
	res, err := api.ActivityController.FindById(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	} else {
		ctx.JSON(http.StatusOK, res)
	}
}

// CreateActivity godoc
// @Security bearerAuth
// @Summary Create new Activitys
// @Description Create a new Activity
// @Tags Activitys,create
// @Accept json
// @Produce json
// @Param Activity body presenter.Activity true "Create Activity"
// @Success 200 {object} dto.Response
// @Failure 400 {object} dto.Response
// @Failure 401 {object} dto.Response
// @Router /activities [post]
func (api *ActivityApi) CreateActivity(ctx *gin.Context) {
	res, err := api.ActivityController.Save(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	} else {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Activity created successfully",
			"result":  res,
		})
	}
}

// UpdateActivity godoc
// @Security bearerAuth
// @Summary Update Activitys
// @Description Update a single Activity
// @Tags Activitys
// @Accept json
// @Produce json
// @Param id path string true "Activity ID"
// @Param Activity body presenter.Activity true "Update Activity"
// @Success 200 {object} dto.Response
// @Failure 400 {object} dto.Response
// @Failure 401 {object} dto.Response
// @Router /activities/{id} [put]
func (api *ActivityApi) UpdateActivity(ctx *gin.Context) {
	res, err := api.ActivityController.Update(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	} else {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Activity update is Valid",
			"result":  res,
		})
	}
}

// DeleteActivity godoc
// @Security bearerAuth
// @Summary Remove Activitys
// @Description Delete a single Activity
// @Tags Activitys
// @Accept json
// @Produce json
// @Param id path string true "Activity ID"
// @Success 200 {object} dto.Response
// @Failure 400 {object} dto.Response
// @Failure 401 {object} dto.Response
// @Router /activities/{id} [delete]
func (api *ActivityApi) DeleteActivity(ctx *gin.Context) {
	res, err := api.ActivityController.Delete(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	} else {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "Activity delete is Valid",
			"result":  res,
		})
	}
}

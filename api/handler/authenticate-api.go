package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/daakghar-service-gin/controller"
)

type AuthenticateApi struct {
	loginController controller.LoginController
}

func NewAuthenticateAPI(loginController controller.LoginController) *AuthenticateApi {
	return &AuthenticateApi{
		loginController: loginController,
	}
}

// Path Information

// Authenticate godoc
// @Summary Provides a JSON web Token
// @Description Authenticates a user and provides a JWT to Authorize API calls
// @ID Authentication
// @Consume application/x-www-form-urlencoded
// @Produce json
// @Param username formData string true "User credentials"
// @Param password formData string true "User credentials"
// @Success 200 {object} dto.JWT
// @Failure 401 {object} dto.Response
// @Router /auth/token [post]
func (api *AuthenticateApi) Authenticate(ctx *gin.Context) {
	token := api.loginController.Login(ctx)
	if token != "" {
		ctx.JSON(http.StatusOK, gin.H{
			"token": token,
		})
	} else {
		ctx.JSON(http.StatusUnauthorized, nil)
	}
}

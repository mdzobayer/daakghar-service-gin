package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/daakghar-service-gin/controller"
)

type ConversationApi struct {
	ConversationController controller.ConversationController
}

func NewConversationAPI(
	ConversationController controller.ConversationController) *ConversationApi {
	return &ConversationApi{
		ConversationController: ConversationController,
	}
}

// Path Information

// GetConversations godoc
// @Security bearerAuth
// @Summary List existing Conversations
// @Description Get all the existing Conversations
// @Tags Conversations, list
// @Accept json
// @Produce json
// @Success 200 {array} conversation.Get
// @Failure 401 {object} dto.Response
// @Router /conversations [get]
func (api *ConversationApi) GetConversations(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, api.ConversationController.FindAll())
}

// GetConversation godoc
// @Security bearerAuth
// @Summary Get existing Conversation
// @Description Get a existing Conversations
// @Tags Conversations
// @Accept json
// @Produce json
// @Param id path string true "Conversation ID"
// @Success 200 {object} conversation.Get
// @Failure 401 {object} dto.Response
// @Router /conversations/{id} [get]
func (api *ConversationApi) GetConversation(ctx *gin.Context) {
	res, err := api.ConversationController.FindById(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	} else {
		ctx.JSON(http.StatusOK, res)
	}
}

// CreateConversation godoc
// @Security bearerAuth
// @Summary Create new Conversations
// @Description Create a new Conversation
// @Tags Conversations,create
// @Accept json
// @Produce json
// @Param Conversation body conversation.Create true "Create Conversation"
// @Success 200 {object} dto.Response
// @Failure 400 {object} dto.Response
// @Failure 401 {object} dto.Response
// @Router /conversations [post]
func (api *ConversationApi) CreateConversation(ctx *gin.Context) {
	res, err := api.ConversationController.Save(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	} else {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "conversation create success",
			"result":  res,
		})
	}
}

// UpdateConversation godoc
// @Security bearerAuth
// @Summary Update Conversations
// @Description Update a single Conversation
// @Tags Conversations
// @Accept json
// @Produce json
// @Param id path string true "Conversation ID"
// @Param Conversation body conversation.Update true "Update Conversation"
// @Success 200 {object} dto.Response
// @Failure 400 {object} dto.Response
// @Failure 401 {object} dto.Response
// @Router /conversations/{id} [put]
func (api *ConversationApi) UpdateConversation(ctx *gin.Context) {
	res, err := api.ConversationController.Update(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	} else {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "conversation update success",
			"result":  res,
		})
	}
}

// DeleteConversation godoc
// @Security bearerAuth
// @Summary Remove Conversations
// @Description Delete a single Conversation
// @Tags Conversations
// @Accept json
// @Produce json
// @Param id path string true "Conversation ID"
// @Success 200 {object} dto.Response
// @Failure 400 {object} dto.Response
// @Failure 401 {object} dto.Response
// @Router /conversations/{id} [delete]
func (api *ConversationApi) DeleteConversation(ctx *gin.Context) {
	res, err := api.ConversationController.Delete(ctx)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	} else {
		ctx.JSON(http.StatusOK, gin.H{
			"message": "conversation delete success",
			"result":  res,
		})
	}
}

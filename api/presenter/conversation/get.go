package conversation

import (
	"gitlab.com/daakghar-service-gin/entity"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Get struct {
	ID                primitive.ObjectID `json:"ItemID"`
	ClientID          string             `json:"ClientID"`
	ConversationName  string             `json:"ConversationName"`
	ConversationAdmin string             `json:"ConversationAdmin"`
	Members           []Member           `json:"Members"`
}

// FromDB sets conversation data from Database
func (c *Get) FromDB(db entity.Conversation) {
	c.ID = db.ID
	c.ClientID = db.ClientID.Hex()
	c.ConversationAdmin = db.ConversationAdmin.Hex()
	c.ConversationName = db.ConversationName

	var jMember Member

	for _, dbMember := range db.Members {
		jMember.FromDB(dbMember)
		c.Members = append(c.Members, Member(jMember))
	}
}

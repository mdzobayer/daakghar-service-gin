package conversation

import (
	"gitlab.com/daakghar-service-gin/entity"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Create struct {
	ClientID          string   `json:"ClientID" binding:"required"`
	ConversationName  string   `json:"ConversationName" binding:"required"`
	ConversationAdmin string   `json:"ConversationAdmin" binding:"required"`
	Members           []Member `json:"Members" binding:"required"`
}

// ToDB sets conversation data from Database
func (m Create) ToDB(db *entity.Conversation) (err error) {

	if len(m.ClientID) > 0 && m.ClientID != "" {
		db.ClientID, err = primitive.ObjectIDFromHex(m.ClientID)
		if err != nil {
			return err
		}
	}

	if len(m.ConversationAdmin) > 0 && m.ConversationAdmin != "" {
		db.ConversationAdmin, err = primitive.ObjectIDFromHex(m.ConversationAdmin)
		if err != nil {
			return err
		}
	}

	if len(m.ConversationName) > 0 && m.ConversationName != "" {
		db.ConversationName = m.ConversationName
	}

	var dbMember entity.Member

	for _, jMember := range m.Members {
		err = jMember.ToDB(&dbMember)
		if err != nil {
			return err
		}
		db.Members = append(db.Members, dbMember)
	}

	return nil
}

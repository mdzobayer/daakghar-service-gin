package conversation

import (
	"gitlab.com/daakghar-service-gin/entity"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Update struct {
	ClientID          string   `json:"ClientID"`
	ConversationName  string   `json:"ConversationName,omitempty"`
	ConversationAdmin string   `json:"ConversationAdmin,omitempty"`
	Members           []Member `json:"Members,omitempty"`
}

// ToDB sets conversation data from Database
func (m Update) ToDB(db *entity.Conversation) (err error) {

	if len(m.ClientID) > 0 && m.ClientID != "" {
		db.ClientID, err = primitive.ObjectIDFromHex(m.ClientID)
		if err != nil {
			return err
		}
	}

	if len(m.ConversationAdmin) > 0 && m.ConversationAdmin != "" {
		db.ConversationAdmin, err = primitive.ObjectIDFromHex(m.ConversationAdmin)
		if err != nil {
			return err
		}
	}

	if len(m.ConversationName) > 0 && m.ConversationName != "" {
		db.ConversationName = m.ConversationName
	}

	var dbMember entity.Member

	for _, jMember := range m.Members {
		err = jMember.ToDB(&dbMember)
		if err != nil {
			return err
		}
		db.Members = append(db.Members, dbMember)
	}

	return nil
}

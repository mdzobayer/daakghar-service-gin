package conversation

import (
	"time"

	"gitlab.com/daakghar-service-gin/entity"
)

// Member holds conversation member info
type Member struct {
	MemberID     string `json:"MemberID" binding:"required"`
	NickName     string `json:"NickName" binding:"required"`
	Role         string `json:"Role" binding:"required"`
	JoinedBy     string `json:"JoinedBy" binding:"required"`
	JoiningTime  string `json:"JoiningTime" binding:"required"`
	RemovedBy    string `json:"RemovedBy,omitempty"`
	RemovingTime string `json:"RemovingTime,omitempty"`
	LeavingTime  string `json:"LeavingTime,omitempty"`
}

// FromDB sets member data from Database
func (m *Member) FromDB(db entity.Member) {
	m.JoinedBy = db.JoinedBy
	m.JoiningTime = db.JoiningTime.String()

	if db.LeavingTime.String() != "0001-01-01 00:00:00 +0000 UTC" {
		m.LeavingTime = db.LeavingTime.String()
	}

	m.MemberID = db.MemberID
	m.NickName = db.NickName
	m.RemovedBy = db.RemovedBy

	if db.RemovingTime.String() != "0001-01-01 00:00:00 +0000 UTC" {
		m.RemovingTime = db.RemovingTime.String()
	}

	m.Role = db.Role
}

// ToDB sets member data from Database
func (m Member) ToDB(db *entity.Member) (err error) {
	db.JoinedBy = m.JoinedBy

	if m.LeavingTime != "" && len(m.LeavingTime) > 0 {

		db.JoiningTime, err = time.Parse(time.RFC3339, m.JoiningTime)
		if err != nil {
			return err
		}

	}

	if m.LeavingTime != "" && len(m.LeavingTime) > 0 {

		db.LeavingTime, err = time.Parse(time.RFC3339, m.LeavingTime)
		if err != nil {
			return err
		}

	}

	db.MemberID = m.MemberID
	db.NickName = m.NickName
	db.RemovedBy = m.RemovedBy

	if m.RemovingTime != "" && len(m.RemovingTime) > 0 {

		db.RemovingTime, err = time.Parse(time.RFC3339, m.RemovingTime)
		if err != nil {
			return err
		}

	}

	db.Role = m.Role

	return
}

package presenter

import (
	"time"

	"gitlab.com/daakghar-service-gin/entity"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Basic holds basic data information
type Basic struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	CreatedAt time.Time          `bson:"CreatedAt,omitempty"`
	UpdatedAt time.Time          `bson:"UpdatedAt,omitempty"`
}

func (b *Basic) FromDB(db entity.Basic) {
	b.ID = db.ID

	b.CreatedAt = db.CreatedAt
	b.UpdatedAt = db.UpdatedAt
}

func (b Basic) ToDB(db *entity.Basic) {
	db.ID = b.ID

	db.CreatedAt = b.CreatedAt
	db.UpdatedAt = b.UpdatedAt
	db.IsDeleted = false
}

package presenter

import (
	"time"

	"gitlab.com/daakghar-service-gin/entity"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Activity holds conversations activity info
type Activity struct {
	ID                          primitive.ObjectID `json:"ItemID,omitempty"`
	ConversationID              string             `json:"ConversationID,omitempty"`
	CreatorID                   string             `json:"CreatorID,omitempty"`
	CreatorDeviceAddress        string             `json:"CreatorDeviceAddress,omitempty"`
	CreatorPublicNetworkAddress string             `json:"CreatorPublicNetworkAddress,omitempty"`
	SeenBy                      []string           `json:"SeenBy,omitempty"`
	MessageBody                 string             `json:"MessageBody,omitempty"`
	Reactions                   []UserReaction     `json:"Reactions,omitempty"`
}

// FromDB sets member data from Database
func (a *Activity) FromDB(db entity.Activity) {
	a.ID = db.ID
	a.ConversationID = db.ConversationID.Hex()
	a.CreatorID = db.CreatorID.Hex()
	a.CreatorDeviceAddress = db.CreatorDeviceAddress
	a.CreatorPublicNetworkAddress = db.CreatorPublicNetworkAddress

	for _, objectID := range db.SeenBy {
		a.SeenBy = append(a.SeenBy, objectID.Hex())
	}

	a.MessageBody = db.MessageBody

	var jUserReaction UserReaction

	for _, dbUserReaction := range db.Reactions {
		jUserReaction.FromDB(dbUserReaction)
		a.Reactions = append(a.Reactions, jUserReaction)
	}
}

// FromDB sets member data from Database
func (a Activity) ToDB(db *entity.Activity) (err error) {
	// db.ID = a.ID

	if len(a.ConversationID) > 0 && a.ConversationID != "" {
		db.ConversationID, err = primitive.ObjectIDFromHex(a.ConversationID)

		if err != nil {
			return err
		}
	}

	if len(a.ConversationID) > 0 && a.ConversationID != "" {
		db.CreatorID, err = primitive.ObjectIDFromHex(a.CreatorID)

		if err != nil {
			return err
		}
	}

	db.CreatorDeviceAddress = a.CreatorDeviceAddress
	db.CreatorPublicNetworkAddress = a.CreatorPublicNetworkAddress

	var objectID primitive.ObjectID

	for _, objectIDHex := range a.SeenBy {

		objectID, err = primitive.ObjectIDFromHex(objectIDHex)

		if err != nil {
			return err
		}

		db.SeenBy = append(db.SeenBy, objectID)
	}

	db.MessageBody = a.MessageBody

	var dbUserReaction entity.UserReaction

	for _, jUserReaction := range a.Reactions {
		err = jUserReaction.ToDB(&dbUserReaction)
		if err != nil {
			return err
		}
		db.Reactions = append(db.Reactions, dbUserReaction)
	}

	return err
}

// UserReaction holds user reaction info
type UserReaction struct {
	UserID       string `json:"UserID"`
	ReactionTime string `json:"ReactionTime"`
	EmojiCode    string `json:"EmojiCode"`
}

// FromDB sets member data from Database
func (u *UserReaction) FromDB(db entity.UserReaction) {
	u.UserID = db.UserID.Hex()

	if db.ReactionTime.String() != "0001-01-01 00:00:00 +0000 UTC" {
		u.ReactionTime = db.ReactionTime.String()
	}

	u.EmojiCode = db.EmojiCode
}

// ToDB sets member data from Database
func (u UserReaction) ToDB(db *entity.UserReaction) (err error) {
	db.UserID, err = primitive.ObjectIDFromHex(u.UserID)

	if err != nil {
		return err
	}

	if u.ReactionTime != "" && len(u.ReactionTime) > 0 {

		db.ReactionTime, err = time.Parse(time.RFC3339, u.ReactionTime)
		if err != nil {
			return err
		}
	}

	db.EmojiCode = u.EmojiCode

	return nil
}

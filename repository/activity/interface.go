package activity

import "gitlab.com/daakghar-service-gin/entity"

type ActivityRepository interface {
	Save(data entity.Activity) (interface{}, error)
	Update(itemId string, data entity.Activity) (interface{}, error)
	Delete(itemId string) (interface{}, error)
	FindAll() ([]entity.Activity, error)
	Find(itemId string) (entity.Activity, error)
	// CloseDB() error
}

package conversation

import "gitlab.com/daakghar-service-gin/entity"

type ConversationRepository interface {
	Save(data entity.Conversation) (interface{}, error)
	Update(itemId string, data entity.Conversation) (interface{}, error)
	Delete(itemId string) (interface{}, error)
	FindAll() ([]entity.Conversation, error)
	Find(itemId string) (entity.Conversation, error)
	// CloseDB() error
}

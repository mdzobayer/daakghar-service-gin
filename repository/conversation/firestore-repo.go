package conversation

// import (
// 	"context"
// 	"log"
// 	"time"

// 	"cloud.google.com/go/firestore"
// 	"gitlab.com/daakghar-service-gin/entity"
// 	"go.mongodb.org/mongo-driver/bson/primitive"
// 	"google.golang.org/api/iterator"
// )

// type firestoreRepo struct{}

// // NewFirestoreRepository
// func NewFirestoreRepository() ConversationRepository {
// 	return &firestoreRepo{}
// }

// func (r *firestoreRepo) Save(data entity.Conversation) (interface{}, error) {
// 	ctx := context.Background()
// 	client, err := firestore.NewClient(ctx, firestoreProjectId)

// 	if err != nil {
// 		log.Fatalf("Failed to create a Firestore client: %v", err)
// 		return nil, err
// 	}

// 	defer client.Close()

// 	_, _, err = client.Collection(firestoreCollectionName).Add(ctx, map[string]interface{}{
// 		"ID":                data.ID,
// 		"ClientID":          data.ClientID,
// 		"ConversationName":  data.ConversationName,
// 		"ConversationAdmin": data.ConversationAdmin,
// 		"Members":           data.Members,
// 	})

// 	if err != nil {
// 		log.Fatalf("Failed to add a new conversation: %v", err)
// 		return nil, err
// 	}

// 	return data, nil
// }

// func (r *firestoreRepo) Find(itemId string) (interface{}, error) {

// 	ctx := context.Background()
// 	client, err := firestore.NewClient(ctx, firestoreProjectId)

// 	if err != nil {
// 		log.Fatalf("Failed to create a Firestore client: %v", err)
// 		return nil, err
// 	}

// 	defer client.Close()

// 	conversation, err := client.Collection(firestoreCollectionName).Doc(itemId).Get(ctx)

// 	if err != nil {
// 		log.Fatalf("failed to find a firestore document: %v", err)
// 		return nil, err
// 	}

// 	return conversation, nil
// }

// func (r *firestoreRepo) FindAll() ([]entity.Conversation, error) {
// 	ctx := context.Background()
// 	client, err := firestore.NewClient(ctx, firestoreProjectId)

// 	if err != nil {
// 		log.Fatalf("Failed to create a Firestore client: %v", err)
// 		return nil, err
// 	}

// 	defer client.Close()
// 	var posts []entity.Conversation

// 	docIterator := client.Collection(firestoreCollectionName).Documents(ctx)

// 	for {
// 		doc, err := docIterator.Next()

// 		if err == iterator.Done {
// 			break
// 		}

// 		if err != nil {
// 			log.Fatalf("Failed to iterate the list of posts: %v", err)
// 			return nil, err
// 		}

// 		conversation := entity.Conversation{
// 			// Basic.ID: doc.Data()["ID"].(primitive.ObjectID),

// 			entity.Basic {
// 				ID: doc.Data()["ID"].(primitive.ObjectID),
// 				CreatedAt: doc.Data()["CreatedAt"].(time.Time),
// 				UpdatedAt: doc.Data()["UpdatedAt"].(time.Time),
// 				IsDeleted: doc.Data()["IsDeleted"].(bool),
// 			},

// 			ConversationName:  doc.Data()["ConversationName"].(string),
// 			ConversationAdmin: doc.Data()["ConversationAdmin"].(string),
// 			Members:           doc.Data()["Members"].([]entity.Member),

// 			entity.Permission: {

// 			},

// 			// "ID":                data.ID,
// 			// "ClientID":          data.ClientID,
// 			// "ConversationName":  data.ConversationName,
// 			// "ConversationAdmin": data.ConversationAdmin,
// 			// "Members":           data.Members,
// 		}

// 		posts = append(posts, conversation)
// 	}

// 	return posts, nil
// }

// func (r *firestoreRepo) Delete(itemId string) (interface{}, error) {
// 	return nil, nil
// }

// func (r *firestoreRepo) Update(data entity.Conversation) (interface{}, error) {
// 	return nil, nil
// }

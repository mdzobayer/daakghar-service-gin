package conversation

import (
	"context"
	"log"
	"time"

	"gitlab.com/daakghar-service-gin/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type mongoRepo struct {
	clientUri     string
	clientOptions *options.ClientOptions
	client        *mongo.Client
}

// NewMongodbRepository
func NewMongoDBRepository(uri string) ConversationRepository {
	return &mongoRepo{
		clientUri: uri,
	}
}

func (r *mongoRepo) prepareMongoClient() (err error) {
	r.clientOptions = options.Client().ApplyURI(r.clientUri)
	r.client, err = mongo.Connect(context.TODO(), r.clientOptions)
	if err != nil {
		log.Println("repository/conversation/mongodb-repo/prepareMongoClient(Connect) Time: ", time.Now().String(), " (client creation failed) : ", err)
		return err
	}

	err = r.client.Ping(context.TODO(), nil)
	if err != nil {
		log.Println("repository/conversation/mongodb-repo/prepareMongoClient(Pint) \t", time.Now().String(), " \t", err)
	}
	return err
}

func (r *mongoRepo) Save(data entity.Conversation) (interface{}, error) {

	err := r.prepareMongoClient()
	if err != nil {
		return nil, err
	}

	// Close Db connection
	defer r.client.Disconnect(context.TODO())

	database := r.client.Database(mongodbDatabaseName)

	collection := database.Collection(mongodbCollectionName)

	insertOneResult, err := collection.InsertOne(
		context.TODO(),
		data,
	)

	if err != nil {
		return nil, err
	}

	return insertOneResult.InsertedID, nil
}

func (r *mongoRepo) Find(itemId string) (entity.Conversation, error) {

	objID, err := primitive.ObjectIDFromHex(itemId)
	if err != nil {
		log.Println("repository/conversation/mongodb-repo/Find \t", time.Now().String(), " \t", err)
		return entity.Conversation{}, err
	}

	err = r.prepareMongoClient()
	if err != nil {
		return entity.Conversation{}, err
	}

	// Close Db connection
	defer r.client.Disconnect(context.TODO())

	database := r.client.Database(mongodbDatabaseName)

	collection := database.Collection(mongodbCollectionName)

	filter := bson.D{bson.E{Key: "_id", Value: objID}}

	var conversation entity.Conversation

	err = collection.FindOne(
		context.TODO(),
		filter,
	).Decode(&conversation)

	if err != nil {
		log.Println("repository/conversation/mongodb-repo/Find \t", time.Now().String(), " \t", err)
		return entity.Conversation{}, err
	}

	return conversation, nil
}

func (r *mongoRepo) FindAll() ([]entity.Conversation, error) {
	return r.GetByFilter(bson.D{{}})
}

func (r *mongoRepo) Delete(itemId string) (interface{}, error) {

	objID, err := primitive.ObjectIDFromHex(itemId)
	if err != nil {
		log.Println("repository/conversation/mongodb-repo/Delete \t", time.Now().String(), " \t", err)
		return nil, err
	}

	err = r.prepareMongoClient()
	if err != nil {
		return nil, err
	}

	// Close Db connection
	defer r.client.Disconnect(context.TODO())

	database := r.client.Database(mongodbDatabaseName)

	collection := database.Collection(mongodbCollectionName)

	// filter prepare
	filter := bson.D{bson.E{Key: "_id", Value: objID}}

	deleteResult, err := collection.DeleteOne(context.TODO(), filter)

	if err != nil {
		return nil, err
	}

	return deleteResult, nil
}

func (r *mongoRepo) Update(itemId string, data entity.Conversation) (interface{}, error) {

	objectId, err := primitive.ObjectIDFromHex(itemId)
	if err != nil {
		log.Println("repository/conversation/mongodb-repo/Update \t", time.Now().String(), " \t", err)
		return nil, err
	}

	err = r.prepareMongoClient()
	if err != nil {
		return nil, err
	}

	// Close Db connection
	defer r.client.Disconnect(context.TODO())

	database := r.client.Database(mongodbDatabaseName)

	collection := database.Collection(mongodbCollectionName)

	// filter prepare for update
	filter := bson.D{bson.E{Key: "_id", Value: objectId}}

	update := bson.D{bson.E{Key: "$set", Value: data}}

	opts := options.Update().SetUpsert(true) // Set Upsert true

	updateResult, err := collection.UpdateOne(context.TODO(), filter, update, opts)

	if err != nil {
		log.Println("repository/conversation/mongodb-repo/Update \t", time.Now().String(), " \t", err)
		return nil, err
	}

	return updateResult, nil
}

func (r *mongoRepo) GetByFilter(filter bson.D) ([]entity.Conversation, error) {

	err := r.prepareMongoClient()
	if err != nil {
		return nil, err
	}

	// Close Db connection
	defer r.client.Disconnect(context.TODO())

	database := r.client.Database(mongodbDatabaseName)

	collection := database.Collection(mongodbCollectionName)

	cursor, err := collection.Find(context.TODO(), filter)

	if err != nil {
		return nil, err
	}

	var conversations []entity.Conversation

	for cursor.Next(context.TODO()) {

		var book entity.Conversation
		err := cursor.Decode(&book)

		if err != nil {
			log.Println("repository/conversation/mongodb-repo/GetByFilter \t", time.Now().String(), " \t", err)
		}

		conversations = append(conversations, book)
	}

	return conversations, nil
}

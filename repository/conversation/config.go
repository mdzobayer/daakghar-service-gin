package conversation

const (
	// firestoreProjectId      string = ""
	// firestoreCollectionName string = ""

	mongodbClientURI      string = "localhost"
	mongodbClientPort     string = "27017"
	mongodbDatabaseName   string = "daakghar"
	mongodbCollectionName string = "conversation"
)

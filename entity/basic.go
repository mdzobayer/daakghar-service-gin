package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Basic holds basic data information
type Basic struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	CreatedAt time.Time          `bson:"CreatedAt"`
	UpdatedAt time.Time          `bson:"UpdatedAt"`
	IsDeleted bool               `bson:"IsDeleted"`
}

// PreparePut prepares Basic before database put operation
func (b *Basic) PreparePut() {
	b.UpdatedAt = time.Now().UTC()

	if b.ID.IsZero() || b.ID.Hex() == "" {
		b.ID = primitive.NewObjectID()
		b.CreatedAt = b.UpdatedAt
	}
}

// PrepareDelete updates delete info
func (b *Basic) PrepareDelete() {
	b.IsDeleted = true
}

// Permission holds data label permission properties
type Permission struct {
	IdsAllowedToRead     []string `bson:"IdsAllowedToRead"`
	IdsAllowedToUpdate   []string `bson:"IdsAllowedToUpdate"`
	IdsAllowedToInsert   []string `bson:"IdsAllowedToInsert"`
	IdsAllowedToDelete   []string `bson:"IdsAllowedToDelete"`
	RolesAllowedRead     []string `bson:"RolesAllowedRead"`
	RolesAllowedInsert   []string `bson:"RolesAllowedInsert"`
	RolesAllowedUpdate   []string `bson:"RolesAllowedUpdate"`
	RolesAllowedToDelete []string `bson:"RolesAllowedToDelete"`
}

package entity

import "go.mongodb.org/mongo-driver/bson/primitive"

// Conversation holds conversation basic info
type Conversation struct {
	Basic             `bson:",inline"`
	ClientID          primitive.ObjectID `bson:"ClientID,omitempty"`
	ConversationName  string             `bson:"ConversationName,omitempty"`
	ConversationAdmin primitive.ObjectID `bson:"ConversationAdmin,omitempty"`
	Members           []Member           `bson:"Members,omitempty"`
	Permission        `bson:",inline,omitempty"`
}

package entity

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Activity holds conversations activity basic info
type Activity struct {
	Basic                       `bson:",inline"`
	ConversationID              primitive.ObjectID   `bson:"ConversationID,omitempty"`
	CreatorID                   primitive.ObjectID   `bson:"CreatorID,omitempty"`
	CreatorDeviceAddress        string               `bson:"CreatorDeviceAddress,omitempty"`
	CreatorPublicNetworkAddress string               `bson:"CreatorPublicNetworkAddress,omitempty"`
	SeenBy                      []primitive.ObjectID `bson:"SeenBy,omitempty"`
	MessageBody                 string               `bson:"MessageBody,omitempty"`
	Reactions                   []UserReaction       `bson:"Reactions,omitempty"`
	Permission                  `bson:",inline"`
}

// UserReaction holds user reaction info
type UserReaction struct {
	UserID       primitive.ObjectID `bson:"UserID"`
	ReactionTime time.Time          `bson:"ReactionTime"`
	EmojiCode    string             `bson:"EmojiCode"`
}

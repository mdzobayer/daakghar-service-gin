package main

import (
	"os"

	"github.com/gin-gonic/gin"
	api "gitlab.com/daakghar-service-gin/api/handler"
	"gitlab.com/daakghar-service-gin/api/middlewares"
	"gitlab.com/daakghar-service-gin/configs"
	"gitlab.com/daakghar-service-gin/controller"
	"gitlab.com/daakghar-service-gin/docs"
	"gitlab.com/daakghar-service-gin/environment"
	"gitlab.com/daakghar-service-gin/repository/activity"
	"gitlab.com/daakghar-service-gin/repository/conversation"
	"gitlab.com/daakghar-service-gin/service"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

var (
	loginService    service.LoginService       = service.NewLoginService()
	jwtService      service.JWTService         = service.NewJWTService()
	loginController controller.LoginController = controller.NewLoginController(loginService, jwtService)

	environmentName string = environment.Local

	conversationRepository conversation.ConversationRepository = conversation.NewMongoDBRepository(configs.GetMongodbUri(environmentName))
	conversationService    service.ConversationService         = service.NewConversationService(conversationRepository)
	conversationController controller.ConversationController   = controller.NewConversationController(conversationService)

	activityRepository activity.ActivityRepository   = activity.NewMongoDBRepository(configs.GetMongodbUri(environmentName))
	activityService    service.ActivityService       = service.NewActivityService(activityRepository)
	activityController controller.ActivityController = controller.NewActivityController(activityService)
)

// func setupLogOutput() {
// 	f, _ := os.Create("calls.log")
// 	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
// }

// @securityDefinitions.apiKey bearerAuth
// @in header
// @name Authorization
func main() {

	// Swagger 2.0 Meta Information
	docs.SwaggerInfo.Title = "DaakGhar - Service"
	docs.SwaggerInfo.Description = "A messaging service"
	docs.SwaggerInfo.Version = "1.0"
	docs.SwaggerInfo.Host = "daakghar-service-gin.herokuapp.com"
	docs.SwaggerInfo.BasePath = "/api/v1"
	docs.SwaggerInfo.Schemes = []string{"https"}

	// setupLogOutput()

	server := gin.New()

	server.Use(
		gin.Recovery(),
		gin.Logger(),
		// middlewares.Logger(),
		// gindump.Dump(),
	)

	server.GET("/status", func(ctx *gin.Context) {
		ctx.JSON(200, gin.H{
			"Status": "OK!",
		})
	})

	authenticateApi := api.NewAuthenticateAPI(loginController)
	conversationAPI := api.NewConversationAPI(conversationController)
	activityAPI := api.NewActivityAPI(activityController)

	apiRoutes := server.Group(docs.SwaggerInfo.BasePath)
	{
		login := apiRoutes.Group("/auth")
		{
			login.POST("/token", authenticateApi.Authenticate)
		}

		conversations := apiRoutes.Group("/conversations", middlewares.AuthorizeJWT())
		{
			conversations.GET("", conversationAPI.GetConversations)
			conversations.GET("/:id", conversationAPI.GetConversation)
			conversations.POST("", conversationAPI.CreateConversation)
			conversations.PUT("/:id", conversationAPI.UpdateConversation)
			conversations.DELETE("/:id", conversationAPI.DeleteConversation)
		}

		activities := apiRoutes.Group("/activities", middlewares.AuthorizeJWT())
		{
			activities.GET("", activityAPI.GetActivities)
			activities.GET("/:id", activityAPI.GetActivity)
			activities.POST("", activityAPI.CreateActivity)
			activities.PUT("/:id", activityAPI.UpdateActivity)
			activities.DELETE("/:id", activityAPI.DeleteActivity)
		}
	}

	server.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	port := os.Getenv("PORT")

	if port == "" {
		port = "5000"
	}
	server.Run(":" + port)
}

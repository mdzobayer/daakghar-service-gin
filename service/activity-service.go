package service

import (
	"gitlab.com/daakghar-service-gin/entity"
	repo "gitlab.com/daakghar-service-gin/repository/activity"
)

type ActivityService interface {
	Save(entity.Activity) (interface{}, error)
	Update(itemId string, activity entity.Activity) (interface{}, error)
	Delete(itemId string) (interface{}, error)
	FindById(itemId string) (entity.Activity, error)
	FindAll() []entity.Activity
}

type activityService struct {
	activityRepository repo.ActivityRepository
}

func NewActivityService(repo repo.ActivityRepository) ActivityService {
	return &activityService{
		activityRepository: repo,
	}
}

func (s *activityService) Save(activity entity.Activity) (interface{}, error) {
	return s.activityRepository.Save(activity)
}

func (s *activityService) FindById(itemId string) (entity.Activity, error) {
	return s.activityRepository.Find(itemId)
}

func (s *activityService) FindAll() []entity.Activity {
	data, _ := s.activityRepository.FindAll()
	return data
}

func (s *activityService) Update(itemId string, activity entity.Activity) (interface{}, error) {
	return s.activityRepository.Update(itemId, activity)
}

func (s *activityService) Delete(itemId string) (interface{}, error) {
	return s.activityRepository.Delete(itemId)
}

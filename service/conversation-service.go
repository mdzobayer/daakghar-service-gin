package service

import (
	"gitlab.com/daakghar-service-gin/entity"
	repo "gitlab.com/daakghar-service-gin/repository/conversation"
)

type ConversationService interface {
	Save(entity.Conversation) (interface{}, error)
	Update(itemId string, conversation entity.Conversation) (interface{}, error)
	Delete(itemId string) (interface{}, error)
	FindById(itemId string) (entity.Conversation, error)
	FindAll() []entity.Conversation
}

type conversationService struct {
	conversationRepository repo.ConversationRepository
}

func NewConversationService(repo repo.ConversationRepository) ConversationService {
	return &conversationService{
		conversationRepository: repo,
	}
}

func (s *conversationService) Save(conversation entity.Conversation) (interface{}, error) {
	return s.conversationRepository.Save(conversation)
}

func (s *conversationService) FindById(itemId string) (entity.Conversation, error) {
	return s.conversationRepository.Find(itemId)
}

func (s *conversationService) FindAll() []entity.Conversation {
	data, _ := s.conversationRepository.FindAll()
	return data
}

func (s *conversationService) Update(itemId string, conversation entity.Conversation) (interface{}, error) {
	return s.conversationRepository.Update(itemId, conversation)
}

func (s *conversationService) Delete(itemId string) (interface{}, error) {
	return s.conversationRepository.Delete(itemId)
}
